package wxpayHelper

import (
	"github.com/gogf/gf/frame/g"
	"github.com/shenghui0779/gochat"
	"github.com/shenghui0779/gochat/mch"
	"github.com/shenghui0779/gochat/oa"
)

// GetWxpay 获取微信底层方法
// isCert 是否加载证书，退款的时候才需要加载
func GetWxpay(isCert bool) *mch.Mch {
	appId := g.Cfg().GetString("pay.wechatpay.appId")
	mchId := g.Cfg().GetString("pay.wechatpay.mchId")
	apiKey := g.Cfg().GetString("pay.wechatpay.apiKey")

	if appId == "" {
		panic("appId 不能未空")
	}
	if mchId == "" {
		panic("mchId 不能未空")
	}
	if apiKey == "" {
		panic("apiKey 不能未空")
	}

	wxpay := gochat.NewMch(appId, mchId, apiKey)

	if isCert {
		err := wxpay.LoadCertificate(mch.WithCertPEMFile("./res/wxcerts/0/apiclient_cert.pem",
			"./res/wxcerts/0/apiclient_key.pem"))
		if err != nil {
			panic(err)
		}
	}

	return wxpay
}

// GetWxOa 获取微信公众号底层方法
func GetWxOa() *oa.OA {
	wxOa := gochat.NewOA(g.Cfg().GetString("pay.wechatpay.appId"), g.Cfg().GetString("pay.wechatpay.appSecret"))

	return wxOa
}
