package OrderQueue

import (
	"github.com/gogf/gf/frame/g"
	"time"
)

// RedisCloseNoPayOrder 关闭未支付订单 入队列
func RedisCloseNoPayOrder() {
	conn := g.Redis("queue").Conn()
	_, err := conn.Do("SUBSCRIBE", "CloseNoPayOrder")
	if err != nil {
		panic(err)
	}

	for {
		reply, err := conn.ReceiveVar()
		if err != nil {
			panic(err)
		}

		r := reply.Ints()
		if err != nil {
			panic(err)
		}

		AddCloseNoPayOrder(r[2])

		time.Sleep(500)
	}
}
