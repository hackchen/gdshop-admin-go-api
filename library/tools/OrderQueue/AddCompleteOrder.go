package OrderQueue

import "github.com/gogf/gf/frame/g"

// AddCompleteOrder 发货成功后 15 天自动完成订单，转成待评价
func AddCompleteOrder(orderId int) (string, error) {
	topicName := "CompleteOrder"
	messageId, err := PushQueue(topicName, OrderQueue{
		OrderId: orderId,
		Type:    5,
	}, g.Cfg().GetInt("queue."+topicName+".delayTime"))
	if err != nil {
		return "", err
	}
	g.Log().Async().Cat("OrderQueue").Info(topicName, " 加入成功 orderId :", orderId)
	return messageId, nil
}
