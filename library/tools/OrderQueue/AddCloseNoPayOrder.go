package OrderQueue

import "github.com/gogf/gf/frame/g"

// AddCloseNoPayOrder 关闭未支付订单 入队列
func AddCloseNoPayOrder(orderId int) (string, error) {
	topicName := "CloseNoPayOrder"
	messageId, err := PushQueue(topicName, OrderQueue{
		OrderId: orderId,
		Type:    0,
	}, g.Cfg().GetInt("queue."+topicName+".delayTime"))
	if err != nil {
		return "", err
	}
	g.Log().Async().Cat("OrderQueue").Info(topicName, " 加入成功 orderId :", orderId)
	return messageId, nil
}
