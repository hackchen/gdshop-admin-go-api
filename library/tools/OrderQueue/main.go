package OrderQueue

import (
	"crypto/md5"
	"fmt"
	"gdshop-admin-go-api/app/service/OrderService"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/util/gconv"
	jsoniter "github.com/json-iterator/go"
	"time"
)

// 统一管理，后面方便替换源
var queueCli Client

// 处理订单队列
func InitOrderQueue() {
	queueCli = Client{
		Conn: g.Redis("queue").Conn(),
	}
	// 关闭订单定时器，按秒执行
	initCloseNoPayOrderQueue()
	// 发货后自动跳到 待评价
	initCompleteOrderQueue()
}

func GetQueueCli() Client {
	return queueCli
}

// 获取下一次执行时间，通过这里控制下次是否执行
func gotoNextExecTime(topic string, data OrderQueue) {
	nextExeTimes := g.Cfg().GetInts("queue." + topic + ".DelayOnFailure")
	if len(nextExeTimes) < 1 {
		return
	}
	// 有设置的话
	if len(nextExeTimes) > data.ReTry {
		delayTime := nextExeTimes[data.ReTry]
		// 重试测试加一
		data.ReTry++
		// 重新加入队列
		_, err := PushQueue(topic, data, delayTime)
		if err != nil {
			glog.Async().Cat("OrderQueue").Error("handleOrderQueue PushQueue error ", err.Error())
			return
		}
	}
}

// PushQueue 统一的加入方法
func PushQueue(topic string, data OrderQueue, delayTime int) (string, error) {
	// 加入队列
	messageId := fmt.Sprintf("%x", md5.Sum([]byte(time.Now().String())))
	body, err := jsoniter.MarshalToString(data)
	if err != nil {
		return "", err
	}
	msg := Message{
		ID:    messageId,
		Topic: topic,
		Body:  body,
	}

	_, err = queueCli.Push(msg, delayTime, 86400)
	if err != nil {
		return "", err
	}

	return messageId, nil
}

// 处理队列
func handleOrderQueue(job *Message) {
	var tmp OrderQueue
	err := gconv.Struct(job.Body, &tmp)
	if err != nil {
		glog.Async().Cat("OrderQueue").Error("handleOrderQueue gconv.Struct error ", err.Error())
		return
	}
	isSuccess := false
	switch tmp.Type {
	case 0:
		jieguo := OrderService.CloseNoPayOrder(nil, tmp.OrderId)
		if jieguo.Code != 0 {
			isSuccess = false
			glog.Async().Cat("OrderQueue").Error("handleOrderQueue exe error Message:",
				jieguo.Message, " data:", tmp)
		} else {
			isSuccess = true
		}
		break
	case 5:
		jieguo := OrderService.CompleteOrder(nil, tmp.OrderId)
		if jieguo.Code != 0 {
			isSuccess = false
			glog.Async().Cat("OrderQueue").Error("handleOrderQueue exe error Message:",
				jieguo.Message, " data:", tmp)
		} else {
			isSuccess = true
		}
		break
	}

	// 执行失败，判断是否有下一次执行
	if isSuccess == false {
		gotoNextExecTime(job.Topic, tmp)
	}
}
