package OrderQueue

import (
	"context"
	"gdshop-admin-go-api/app/request/OrderReq"
	"gdshop-admin-go-api/app/service/OrderService"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/util/gconv"
	"time"
)

// MemberCloseOrder 会员关闭订单 Redis 监听
func MemberCloseOrder() {
	conn := g.Redis("queue").Conn()
	_, err := conn.Do("SUBSCRIBE", "MemberCloseOrder")
	if err != nil {
		panic(err)
	}

	for {
		reply, err := conn.ReceiveVar()
		if err != nil {
			panic(err)
		}

		r := reply.Strings()
		if err != nil {
			panic(err)
		}
		var tmp OrderReq.MemberCloseOrder
		err = gconv.Struct(r[2], &tmp)
		if err != nil {
			g.Log().Async().Cat("MemberCloseOrder").Error("gconv.Struct 失败：" + err.Error())
			return
		}
		chuliMemberCloseOrder(tmp)

		time.Sleep(500)
	}
}

func chuliMemberCloseOrder(params OrderReq.MemberCloseOrder) {
	ctx := context.Background()
	jieguo := OrderService.MemberCloseOrder(ctx, &params)

	if jieguo.Code == 0 {
		OrderService.GiveBackCouponByOrderId(ctx, params.OrderId)
	}
}
