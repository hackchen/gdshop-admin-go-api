package myCache

import (
	"github.com/gogf/gcache-adapter/adapter"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/gcache"
	"time"
)

func GetOrSetFunc(key interface{}, f func() (interface{}, error), duration time.Duration) (interface{}, error) {
	c := gcache.New()
	c.SetAdapter(adapter.NewRedis(g.Redis()))
	return c.GetOrSetFunc(key, f, duration)
}
