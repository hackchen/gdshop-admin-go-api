package version

import "github.com/gogf/gf/net/ghttp"

func GetVersion(r *ghttp.Request, versionName string) string {
	version := r.Header.Get(versionName)
	if len(version) > 0 {
		return version
	} else {
		return "1.0.0"
	}
}
