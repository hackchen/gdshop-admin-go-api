package cacheHelp

import (
	"fmt"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/util/gconv"
)

// GetAdminCaches 获取管理员缓存
func GetAdminCaches(adminId string) []string {
	return g.ArrayStr{
		"adminCache:model:" + adminId,      // 权限
		"adminCache:depaName:" + adminId,   // 菜单
		"adminCache:perms:" + adminId,      // 部门名称
		"adminCache:menu:" + adminId,       // 角色
		"adminCache:roleIdList:" + adminId, // 单条信息
	}
}

// ClaerAdminCaches 清除管理员缓存
func ClaerAdminCaches(adminId string) {
	fmt.Println("ClaerAdminCaches  ", GetAdminCaches(adminId))
	_, err := g.DB().GetCache().Remove(gconv.Interfaces(GetAdminCaches(adminId))...)
	if err != nil {
		fmt.Println(err)
		return
	}
}
