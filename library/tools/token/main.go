package token

import (
	"gdshop-admin-go-api/library/tools/jwt"
	"github.com/gogf/gf/net/ghttp"
)

func GetLoginMemberId(r *ghttp.Request) int {
	return r.GetCtxVar("memberId", 0).Int()
}

func GetToken(r *ghttp.Request) string {
	token := r.Header.Get("token")
	return token
}

// 检测token
func CheckToken(r *ghttp.Request) bool {
	/*
		if g.Cfg().GetBool("site.Debug") == true {
			r.SetCtxVar("memberId", 5)
			return true
		}
	*/
	token := GetToken(r)
	if token == "" {
		return false
	}
	memberId := jwt.VerifyLoginToken(token)

	if memberId > 0 {
		// 写入全局上下文
		r.SetCtxVar("memberId", memberId)
		return true
	} else {
		// 如果是 0 说明是未登陆的
		return false
	}
}
