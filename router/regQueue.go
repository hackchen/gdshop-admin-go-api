package router

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"github.com/gogf/gf/net/ghttp"
)

func regQueue(s *ghttp.Server, versionName, defaultVersionName string) {
	// 路由前缀
	routerPrefix := "/gdshop"
	s.Group(routerPrefix+"/queue", func(group *ghttp.RouterGroup) {

		c := controllers.NewQueueController(&BaseReq.I{})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		//group.POST("/add_queue", func(r *ghttp.Request) {
		//	BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "AddQueue")
		//})

		group.GET("/member_pay", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "MemberPay")
		})
		group.POST("/member_pay", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "PostMemberPay")
		})
		group.GET("/member_close_order", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "MemberCloseOrder")
		})
		group.GET("/add_queue_close_order", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "AddQueueCloseOrder")
		})

		group.GET("/add_complete_order", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "AddCompleteOrder")
		})
	})
}
