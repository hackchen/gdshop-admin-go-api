package router

import (
	"gdshop-admin-go-api/app/controllers/shop"
	"gdshop-admin-go-api/app/request/BaseReq"
	"github.com/gogf/gf/net/ghttp"
)

func regPayNotify(s *ghttp.Server, versionName, defaultVersionName string) {
	// 路由前缀
	routerPrefix := "/notify"
	s.Group(routerPrefix, func(group *ghttp.RouterGroup) {
		c := shop.NewPayNotifyController(&BaseReq.I{})
		//controllerMaps := map[string]interface{}{
		//	"1.0.0": c,
		//}
		// 微信公众号，支付回调
		group.POST("/wechatpay_oa", func(r *ghttp.Request) {
			c.WechatpayOa(r)
		})
		// 微信公众号，退款回调
		group.POST("/wechatpay_refund_oa", func(r *ghttp.Request) {
			c.WechatpayRefundOa(r)
		})
	})
}
