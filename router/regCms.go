package router

import (
	"gdshop-admin-go-api/app/controllers/cms"
	"gdshop-admin-go-api/app/request/BaseReq"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func regCms(s *ghttp.Server, versionName, defaultVersionName string) {
	// 路由前缀
	routerPrefix := "/cms"

	// 注册路由 - 文章
	s.Group(routerPrefix+"/article", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		var leftJoin []*BaseReq.QueryOpLeftJoin
		dbPrefix := g.Cfg().GetString("database.prefix")
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "category c",
			Condition: "a.category_id = c.id",
		})

		c := cms.NewArticleController(&BaseReq.I{
			TableName:          "article",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				SelectFields: g.ArrayStr{
					"a.*",
					"c.cat_name",
				},
				AsName:   "a",
				LeftJoin: leftJoin,
				KeyWordLikeFields: []string{
					"a.title",
				},
				OtherWhere: func(r *ghttp.Request) g.Map {
					data := g.Map{}
					categoryIds := r.GetArray("categoryIds")
					if len(categoryIds) > 0 {
						data["category_id IN (?)"] = categoryIds
					}

					return data
				},
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)

		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/move", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Move")
		})
	})
	// 注册路由 - 幻灯片
	s.Group(routerPrefix+"/slide", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		var leftJoin []*BaseReq.QueryOpLeftJoin
		dbPrefix := g.Cfg().GetString("database.prefix")
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "category c",
			Condition: "s.category_id = c.id",
		})

		c := cms.NewSlideController(&BaseReq.I{
			TableName:          "slides",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				SelectFields: g.ArrayStr{
					"s.*",
					"c.cat_name",
				},
				AsName: "s",
				KeyWordLikeFields: []string{
					"s.title",
				},
				LeftJoin: leftJoin,
				OrderBy:  "s.sort DESC",
				OtherWhere: func(r *ghttp.Request) g.Map {
					data := g.Map{}
					categoryIds := r.GetArray("categoryIds")
					if len(categoryIds) > 0 {
						data["category_id IN (?)"] = categoryIds
					}

					return data
				},
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/move", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Move")
		})
	})
	// 注册路由 - 友情链接
	s.Group(routerPrefix+"/links", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		var leftJoin []*BaseReq.QueryOpLeftJoin
		dbPrefix := g.Cfg().GetString("database.prefix")
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "category c",
			Condition: "l.category_id = c.id",
		})

		c := cms.NewLinkController(&BaseReq.I{
			TableName:          "links",
			InfoIgnoreProperty: "update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				SelectFields: g.ArrayStr{
					"l.*",
					"c.cat_name",
				},
				AsName: "l",
				KeyWordLikeFields: []string{
					"l.title",
				},
				LeftJoin: leftJoin,
				OtherWhere: func(r *ghttp.Request) g.Map {
					data := g.Map{}
					categoryIds := r.GetArray("categoryIds")
					if len(categoryIds) > 0 {
						data["category_id IN (?)"] = categoryIds
					}

					return data
				},
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/move", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Move")
		})
	})
}
