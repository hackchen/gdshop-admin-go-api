package router

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func regGdshop(s *ghttp.Server, versionName, defaultVersionName string) {

	s.Group("/gdshop/attachmentGroup", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewAttachmentGroupController(&BaseReq.I{
			TableName:          "attachment_group",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			/*ListQueryOp: &BaseReq.QueryOp{
				IsCache: 86400,
			},*/
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	s.Group("/gdshop/attachment", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewAttachmentController(&BaseReq.I{
			TableName:          "attachment",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				SelectFields: []string{
					"extension",
					"group_id",
					"id",
					"mime",
					"original_name",
					"save_name",
					"save_path",
					"size",
					"type",
					"url",
				},
				OtherWhere: func(r *ghttp.Request) g.Map {
					groupId := r.GetInt("group_id")
					otherWhere := g.Map{}
					if groupId > 0 {
						otherWhere["group_id"] = groupId
					}
					return otherWhere
				},
				OrderBy: "id DESC",
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		//group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/upload", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Upload")
		})

		group.GET("/get_base_url", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetBaseUrl")
		})

		group.POST("/move", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Move")
		})

		group.POST("/remote_photo", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "RemotePhoto")
		})
	})

	s.Group("/gdshop/configs", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewConfigsController(&BaseReq.I{
			TableName:          "configs",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				OrderBy: "id DESC",
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	s.Group("/gdshop/menu", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewMenuController(&BaseReq.I{
			TableName:          "sys_menu",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				OrderBy: "sort ASC,id DESC",
			},
			ListQueryOp: &BaseReq.QueryOp{
				OrderBy: "sort ASC,id DESC",
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	s.Group("/gdshop/role", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewRoleController(&BaseReq.I{
			TableName:          "sys_role",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				OrderBy: "id DESC",
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	s.Group("/gdshop/department", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewDepartmentController(&BaseReq.I{
			TableName:          "sys_department",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				OrderBy: "id DESC",
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/order", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Order")
		})
	})

	s.Group("/gdshop/open", func(group *ghttp.RouterGroup) {
		c := controllers.NewOpenController(&BaseReq.I{
			TableName:          "sys_department",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				OrderBy: "id DESC",
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		/*regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)*/

		group.GET("/captcha", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Captcha")
		})

		group.POST("/login", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Login")
		})
	})

	s.Group("/gdshop/log", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewLogController(&BaseReq.I{
			TableName:          "app_request_log",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				OrderBy: "id DESC",
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.GET("/getKeep", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetKeep")
		})

		group.POST("/setKeep", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "SetKeep")
		})

		group.POST("/clear", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Clear")
		})
	})

	s.Group("/gdshop/common", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewCommonController(&BaseReq.I{})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)

		group.GET("/areaTree", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "AreaTree")
		})

		group.GET("/areaList", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "AreaList")
		})

		group.GET("/areaLevelList", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "AreaLevelList")
		})
	})
}
