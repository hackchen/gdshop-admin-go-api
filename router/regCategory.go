package router

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func regCategory(s *ghttp.Server, versionName, defaultVersionName string) {
	// 注册 CMS 分类
	s.Group("/cms/category", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewCategoryController(&BaseReq.I{
			TableName:          "category",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			UpdateIgnoreProperty: []string{
				"project_id",
				"ancestors",
			},
			ListQueryOp: &BaseReq.QueryOp{
				SelectFields: g.ArrayStr{
					"id",
					"type",
					"parent_id",
					"cat_name",
					"letter",
					"cover",
					"jump_url",
					"sort",
				},
				FieldsEq: g.ArrayStr{
					"type",
				},
				OrderBy: "sort DESC,id ASC",
				OtherWhere: func(r *ghttp.Request) g.Map {
					return map[string]interface{}{
						"type IN (?)": g.Slice{"1", "2", "3", "4"},
					}
				},
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/move", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Move")
		})

		group.POST("/order", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Order")
		})
	})

	// 注册商品分类
	s.Group("/shop/category", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)
		c := controllers.NewCategoryController(&BaseReq.I{
			TableName:          "category",
			InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
			UpdateIgnoreProperty: []string{
				"project_id",
				"ancestors",
			},
			ListQueryOp: &BaseReq.QueryOp{
				SelectFields: g.ArrayStr{
					"id",
					"type",
					"parent_id",
					"cat_name",
					"letter",
					"cover",
					"jump_url",
					"sort",
				},
				FieldsEq: g.ArrayStr{
					"type",
				},
				OrderBy: "sort DESC,id ASC",
				OtherWhere: func(r *ghttp.Request) g.Map {
					return map[string]interface{}{
						"type": 5,
					}
				},
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/move", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Move")
		})

		group.POST("/order", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Order")
		})
	})
}
