package router

import (
	"gdshop-admin-go-api/app/controllers/shop"
	"gdshop-admin-go-api/app/request/BaseReq"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func regFenxiao(s *ghttp.Server, versionName, defaultVersionName string) {
	// 路由前缀
	routerPrefix := "/fenxiao"
	s.Group(routerPrefix+"/config", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewFenxiaoController(&BaseReq.I{
			TableName: "fenxiao",
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"update",
			"info",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.GET("/surveys", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
				"Surveys")
		})
	})

	s.Group(routerPrefix+"/goods", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewFenxiaoController(&BaseReq.I{
			TableName: "fenxiao_goods",
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)

		group.POST("/add", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
				"AddGoods")
		})

		group.POST("/page", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
				"GetGoodsPage")
		})

		group.POST("/delete", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
				"Delete")
		})

		group.POST("/change_status", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
				"ChangeStatus")
		})
	})

	s.Group(routerPrefix+"/goodsLevel", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewFenxiaoGoodsLevelController(&BaseReq.I{
			TableName: "fenxiao_goods_level",
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"update",
			"info",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	s.Group(routerPrefix+"/level", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewFenxiaoController(&BaseReq.I{
			TableName: "fenxiao",
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)

		group.GET("/info", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
				"GetLevel")
		})
		group.POST("/update", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
				"SetLevel")
		})
	})

	s.Group(routerPrefix+"/order", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewFenxiaoOrderController(&BaseReq.I{
			TableName: "fenxiao_order",
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)

		// 注册基础方法
		regBase([]string{
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	s.Group(routerPrefix+"/user", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewFenxiaoUserController(&BaseReq.I{
			TableName: "fenxiao_user",
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)

		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/setStatus", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "SetStatus")
		})
	})
	s.Group(routerPrefix+"/userInvoice", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		var leftJoin []*BaseReq.QueryOpLeftJoin
		dbPrefix := g.Cfg().GetString("database.prefix")
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "member m",
			Condition: "fui.member_id = m.id",
		})
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "fenxiao_user fu",
			Condition: "fui.fenxiao_user_id = fu.id",
		})

		c := shop.NewFenxiaoUserInvoiceController(&BaseReq.I{
			TableName: "fenxiao_user_invoice",
			PageQueryOp: &BaseReq.QueryOp{
				OrderBy: "id DESC",
				FieldsEq: []string{
					"fui.invoice_type",
				},
				KeyWordLikeFields: []string{
					"fu.name",
					"m.nickname",
				},
				AsName: "fui",
				SelectFields: []string{
					"fui.*",
					"fu.name AS fu_name",
					"m.nickname AS m_nickname",
				},
				LeftJoin: leftJoin,
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)

		// 注册基础方法
		regBase([]string{
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	s.Group(routerPrefix+"/withdraw", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewFenxiaoWithdrawController(&BaseReq.I{
			TableName: "fenxiao_withdraw",
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)

		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/change_handle", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "ChangeHandle")
		})
	})
}
