package router

import (
	"fmt"
	"gdshop-admin-go-api/library/event"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"gdshop-admin-go-api/library/tools/token"
	version "gdshop-admin-go-api/library/tools/version"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"reflect"
	"regexp"
	"sort"
	"strings"
	"time"
)

// 调用方法
func methodCall(r *ghttp.Request, version string, controller interface{}, methodName string) *response.JsonResponse {
	t := reflect.ValueOf(controller)
	// 获取当前需要执行的方法
	currentMethod := t.MethodByName(methodName)
	// 判断方法是否存在
	if !currentMethod.IsValid() {
		// 清除输出的信息
		r.Response.ClearBuffer()
		return response.FailByRequestMessage(r, version+" 不存在该方法，请确认后重新提交")
	}
	// 执行的参数，既是 请求
	args := []reflect.Value{reflect.ValueOf(r)}
	// 前置方法
	initMethod := t.MethodByName("Init")
	if initMethod.IsValid() {
		jieguo, ok := initMethod.Call(args)[0].Interface().(*response.JsonResponse)
		if !ok {
			r.Response.ClearBuffer()
			return response.FailByRequestMessage(r, "Init 返回 类型错误")
		}
		if jieguo.Code != 0 {
			return jieguo
		}
	}
	// 执行当前URL方法
	jsonResp, ok := currentMethod.Call(args)[0].Interface().(*response.JsonResponse)
	if !ok {
		r.Response.ClearBuffer()
		return response.FailByRequestMessage(r, methodName+" 返回 类型错误")
	}
	if jsonResp == nil {
		r.Response.ClearBuffer()
		return response.FailByRequestMessage(r, methodName+" 返回 nil")
	}
	jsonResp.Version = version
	//r.Response.WriteJson(jsonResp)
	// 后置方法
	shutMethod := t.MethodByName("Shut")
	if shutMethod.IsValid() {
		jieguo, ok := initMethod.Call(args)[0].Interface().(*response.JsonResponse)
		if !ok {
			r.Response.ClearBuffer()
			return response.FailByRequestMessage(r, "Shut 返回 类型错误")
		}
		if jieguo.Code != 0 {
			// 清除输出的信息
			r.Response.ClearBuffer()
			return jieguo
		}
	}
	// 返回执行方法
	return jsonResp
}

// 获取控制器映射里面的全部 keys 并做倒序返回
func getControllerMapAllKeys(m map[string]interface{}) []string {
	j := 0
	keys := make([]string, len(m))
	for k := range m {
		keys[j] = k
		j++
	}
	sort.Slice(keys, func(i, j int) bool {
		return keys[i] > keys[j]
	})
	return keys
}

// BindHandlerVersion 绑定处理方法和版本
func BindHandlerVersion(r *ghttp.Request, versionName string, controllerMaps map[string]interface{},
	defaultVersionName string, methodName string) {

	var jsonResp *response.JsonResponse
	var versionVal string
	versionNames := getControllerMapAllKeys(controllerMaps)
	if len(versionNames) < 1 {
		response.JsonExit(r, -1, "controllerMaps 为空，请联系管理员")
		return
	}

	// 如果 controllerMaps 赋值长度只有一个，直接调用
	if len(controllerMaps) == 1 {
		versionVal = versionNames[0]
		jsonResp = methodCall(r, versionVal, controllerMaps[versionVal], methodName)
	} else {
		versionVal = version.GetVersion(r, versionName)
		// 如果未传 version ，则使用默认的版本
		if versionVal == "" {
			versionVal = defaultVersionName
		}
		//
		controller, exists := controllerMaps[versionVal]
		if !exists {
			// 降级使用低级版本
			// 自动取最新的一个版本
			versionVal = versionNames[0]
			controller, exists := controllerMaps[versionVal]
			if !exists {
				// 降级版本不存在
				response.JsonExit(r, -1, "已使用降级版本"+versionVal+"，还是未存在")
				return
			} else {
				//fmt.Println("---------- 使用降级版本"+versionVal+" -------------")
				// 存在
				r.Header.Add(versionName, versionVal)
				// 使用默认版本传入
				jsonResp = methodCall(r, versionVal, controller, methodName)
			}
		} else {
			jsonResp = methodCall(r, versionVal, controller, methodName)
		}
	}

	// 日志
	if strings.Index(r.Request.URL.Path, "/upload") == -1 {
		// 写请求日志，根据配置
		if g.Cfg().GetBool("site.RequestLogIsOpen") {
			addReqLog(r, r.Request.URL.Path, versionVal, jsonResp)
		}
		event.Trigger(strings.ReplaceAll(strings.Trim(r.Request.URL.Path, "/"), "/", "."), nil)
	}
	// 根据URL处理缓存，自动清除缓存
	//claerCacheByUrl(r.Request.URL.Path)

	err := r.Response.WriteJsonExit(jsonResp)
	if err != nil {
		return
	}
}

// 写日志，暂时不记录返回数据
func addReqLog(r *ghttp.Request, path, version string, jsonResp *response.JsonResponse) {
	/*returnData, err := json.Marshal(jsonResp)
	jsonText := ""
	if err != nil {
		jsonText = "json err"
	} else {
		jsonText = string(returnData)
	}*/
	if strings.Index(path, "/gdshop/open/captcha") != -1 {
		return
	}
	_, _ = toolsDb.GetUnSafaTableAddDeleteWhere(r.GetCtx(), "app_request_log").Insert(g.Map{
		"user_id":     token.GetLoginMemberId(r),
		"request_id":  r.GetCtxVar("trace_id"),
		"method_type": r.Method,
		"route":       path,
		//"header_data" : "{}",
		"param_data": r.GetMap(),
		/*"return_data": g.Map{
			"code":    jsonResp.Code,
			"message": jsonResp.Message,
		},*/
		"return_code":    jsonResp.Code,
		"return_message": jsonResp.Message,
		"version":        version,
		"ip":             r.GetRemoteIp(),
		"in_at":          time.Now().UnixNano() / 1e6,
	})
}

// 注册基础方法
func regBase(apis []string, group *ghttp.RouterGroup, controllerMaps map[string]interface{}, versionName,
	defaultVersionName string) {
	for _, apiName := range apis {
		switch apiName {
		case "add":
			group.POST("/"+apiName, func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
					"Add")
			})
			break
		case "update":
			group.POST("/update", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
					"Update")
			})
			break
		case "delete":
			group.POST("/delete", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
					"Delete")
			})
			break
		case "info":
			group.GET("/info", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
					"Info")
			})
			break
		case "list":
			group.POST("/list", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
					"List")
			})
			break
		case "page":
			group.POST("/page", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName,
					"Page")
			})
			break
		}
	}
}

var claerCacheUrls = map[string]string{
	`^/gdshop/admin/update`: "adminCache:*",
	`^/gdshop/role/add`:     "adminCache:*",
	`^/gdshop/role/update`:  "adminCache:*",
	`^/gdshop/role/delete`:  "adminCache:*",
	`^/gdshop/menu/add`:     "adminCache:*",
	`^/gdshop/menu/update`:  "adminCache:*",
	`^/gdshop/menu/delete`:  "adminCache:*",
}

func claerCacheByUrl(url string) {
	for k, item := range claerCacheUrls {
		reg1 := regexp.MustCompile(k)
		result1 := reg1.FindAllStringSubmatch(url, -1)
		if len(result1) > 0 {
			doVar, err := g.Redis().DoVar("keys", item)
			if err != nil {
				fmt.Println(err)
				return
			}
			g.Redis().Do("del", doVar.Array()...)
		}
	}
}
