
   _____   _____    ____                _____   ______
  / ____| |  __ \  |  _ \      /\      / ____| |  ____|
 | |  __  | |  | | | |_) |    /  \    | (___   | |__
 | | |_ | | |  | | |  _ <    / /\ \    \___ \  |  __|
 | |__| | | |__| | | |_) |  / ____ \   ____) | | |____
  \_____| |_____/  |____/  /_/    \_\ |_____/  |______|



GoVersion: {{ .GoVersion }}
GoFrameVersion: {{ .GoFrameVersion }}
GdbaseVersion: v6.{{ .GdbaseVersion }}
OS: {{ .GOOS }}
GOARCH: {{ .GOARCH }}
NumCPU: {{ .NumCPU }}
GOPATH: {{ .GOPATH }}
GOROOT: {{ .GOROOT }}
Compiler: {{ .Compiler }}
ENV: {{ .t.Env "GOPATH" }}
Now: {{ .t.Now "2006-01-02 15:04:05" }}

路由映射表：