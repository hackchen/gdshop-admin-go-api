package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/MemberService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type MemberController struct {
	*BaseController
}

func NewMemberController(inputReq *BaseReq.I) *MemberController {
	return &MemberController{
		&BaseController{Req: inputReq},
	}
}

func (c *MemberController) SetStatus(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return MemberService.SetStatus(r, c.Req)
}

func (c *MemberController) ReSetPwd(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return MemberService.ReSetPwd(r, c.Req)
}
