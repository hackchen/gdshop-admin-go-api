package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/OpenReq"
	"gdshop-admin-go-api/app/service/OpenService"
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools/myCaptcha"
	"github.com/gogf/gf/net/ghttp"
)

type OpenController struct {
	*BaseController
}

func NewOpenController(inputReq *BaseReq.I) *OpenController {
	return &OpenController{
		&BaseController{Req: inputReq},
	}
}

// Login 登陆
func (c *OpenController) Login(r *ghttp.Request) *response.JsonResponse {
	var parames *OpenReq.Login
	if err := r.Parse(&parames); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return OpenService.Login(r, parames)
}

// Captcha 生成验证码图片
func (c *OpenController) Captcha(r *ghttp.Request) *response.JsonResponse {

	return myCaptcha.CreateCaptcha(110, 36)
	/*drive := base64Captcha.NewDriverDigit(36,
		110, 4, 0, 50)
	cap := base64Captcha.NewCaptcha(drive,
		base64Captcha.DefaultMemStore)

	id, b64s, err := cap.Generate()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}

	return response.SuccessByRequestMessageData(r, "成功", g.Map{
		"captchaId": id,
		"img":       b64s,
	})*/
}
