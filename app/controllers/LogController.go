package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/LogService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type LogController struct {
	*BaseController
}

func NewLogController(inputReq *BaseReq.I) *LogController {
	return &LogController{
		&BaseController{Req: inputReq},
	}
}

func (c *LogController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return LogService.Page(r, c.Req)
}

func (c *LogController) GetKeep(r *ghttp.Request) *response.JsonResponse {
	return response.SuccessByRequestMessageData(r, "成功", 31)
}

func (c *LogController) SetKeep(r *ghttp.Request) *response.JsonResponse {
	return response.SuccessByRequestMessageData(r, "成功", 31)
}

func (c *LogController) Clear(r *ghttp.Request) *response.JsonResponse {
	return LogService.Clear(r, c.Req)
}
