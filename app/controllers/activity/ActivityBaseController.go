package activity

import (
	"gdshop-admin-go-api/app/controllers"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gvalid"
)

type ActivityBaseController struct {
	*controllers.BaseController
}

func (c *ActivityBaseController) checkId(r *ghttp.Request) error {
	c.Req.Id = r.GetString("id", "")
	if err := gvalid.CheckValue(r.GetCtx(), c.Req.Id, "required", "ID不能为空"); err != nil {
		return err
	}

	return nil
}
func (c *ActivityBaseController) checkIds(r *ghttp.Request) error {
	c.Req.Ids = r.GetArray("ids")
	return nil
}
