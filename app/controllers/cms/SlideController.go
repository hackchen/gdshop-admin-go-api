package cms

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
)

type SlideController struct {
	*CmsBaseController
}

func NewSlideController(inputReq *BaseReq.I) *SlideController {
	return &SlideController{
		&CmsBaseController{&controllers.BaseController{Req: inputReq}},
	}
}
