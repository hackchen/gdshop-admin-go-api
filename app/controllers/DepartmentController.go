package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/DeptReq"
	"gdshop-admin-go-api/app/service/DeptService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type DepartmentController struct {
	*BaseController
}

func NewDepartmentController(inputReq *BaseReq.I) *DepartmentController {
	return &DepartmentController{
		&BaseController{Req: inputReq},
	}
}

func (c *DepartmentController) Order(r *ghttp.Request) *response.JsonResponse {
	var parames *DeptReq.Order
	if err := r.Parse(&parames); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	//parames.MemberId = token.GetLoginMemberId(r)
	return DeptService.Order(r, parames)
}
