package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/AreaService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type CommonController struct {
	*BaseController
}

func NewCommonController(inputReq *BaseReq.I) *CommonController {
	return &CommonController{
		&BaseController{Req: inputReq},
	}
}

func (c *CommonController) AreaTree(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return AreaService.Tree(r, c.Req)
}

func (c *CommonController) AreaList(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return AreaService.List(r, c.Req)
}

func (c *CommonController) AreaLevelList(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return AreaService.LevelList(r, c.Req)
}
