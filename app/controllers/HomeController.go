package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type HomeController struct {
	*BaseController
}

func NewHomeController(inputReq *BaseReq.I) *HomeController {
	return &HomeController{
		&BaseController{Req: inputReq},
	}
}

func (c *HomeController) Index(r *ghttp.Request) *response.JsonResponse {
	return response.SuccessByRequest(r)
}
