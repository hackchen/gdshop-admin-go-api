package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
)

// FenxiaoGoodsController 分销
type FenxiaoGoodsController struct {
	*ShopBaseController
}

func NewFenxiaoGoodsController(inputReq *BaseReq.I) *FenxiaoGoodsController {
	return &FenxiaoGoodsController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}
