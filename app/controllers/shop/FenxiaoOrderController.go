package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/FenxiaoOrderService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// FenxiaoOrderController 分销
type FenxiaoOrderController struct {
	*ShopBaseController
}

func NewFenxiaoOrderController(inputReq *BaseReq.I) *FenxiaoOrderController {
	return &FenxiaoOrderController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *FenxiaoOrderController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return FenxiaoOrderService.Page(r, c.Req)
}
