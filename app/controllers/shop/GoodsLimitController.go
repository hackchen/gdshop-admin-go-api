package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/GoodsService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type GoodsLimitController struct {
	*ShopBaseController
}

func NewGoodsLimitController(inputReq *BaseReq.I) *GoodsLimitController {
	inputReq.TableName = "goods_limit"
	return &GoodsLimitController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *GoodsLimitController) GetLimitRules(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsService.GetLimitRules(r, c.Req)
}

func (c *GoodsLimitController) UpdateLimitRules(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsService.UpdateLimitRules(r, c.Req)
}
