package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/OrderReq"
	"gdshop-admin-go-api/app/service/OrderService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// OrderController 订单
type OrderController struct {
	*ShopBaseController
}

func NewOrderController(inputReq *BaseReq.I) *OrderController {
	return &OrderController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *OrderController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return OrderService.Page(r, c.Req)
}

func (c *OrderController) Info(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return OrderService.Info(r.GetCtx(), c.Req)
}

func (c *OrderController) ChangeRemark(r *ghttp.Request) *response.JsonResponse {
	var params *OrderReq.ChangeRemark
	if err := r.Parse(&params); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	params.AdminId = c.Req.AdminId
	return OrderService.ChangeRemark(r.GetCtx(), params)
}

func (c *OrderController) ChangeStatus(r *ghttp.Request) *response.JsonResponse {
	var params *OrderReq.ChangeStatus
	if err := r.Parse(&params); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	params.AdminId = c.Req.AdminId
	return OrderService.ChangeStatus(r.GetCtx(), params)
}

func (c *OrderController) SetPriceChange(r *ghttp.Request) *response.JsonResponse {
	var params *OrderReq.SetPriceChange
	if err := r.Parse(&params); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	params.AdminId = c.Req.AdminId
	return OrderService.SetPriceChange(r.GetCtx(), params)
}

func (c *OrderController) GetAddressInfo(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return OrderService.GetAddressInfo(r.GetCtx(), c.Req)
}

func (c *OrderController) GetExpressInfo(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return OrderService.GetExpressInfo(r.GetCtx(), c.Req)
}

func (c *OrderController) GetExpressInfoList(r *ghttp.Request) *response.JsonResponse {
	return OrderService.GetExpressInfoList(r.GetCtx(), r.GetString("sn"))
}

func (c *OrderController) GetListTypes(r *ghttp.Request) *response.JsonResponse {
	return OrderService.ListTypes(r.GetCtx(), c.Req)
}

func (c *OrderController) DiscountInfo(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return OrderService.DiscountInfo(r.GetCtx(), c.Req)
}
