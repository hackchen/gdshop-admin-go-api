package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
)

// StoreController 店铺
type StoreController struct {
	*ShopBaseController
}

func NewStoreController(inputReq *BaseReq.I) *StoreController {
	return &StoreController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}
