package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
)

// ExpressController 快递信息
type ExpressController struct {
	*ShopBaseController
}

func NewExpressController(inputReq *BaseReq.I) *ExpressController {
	inputReq.TableName = "express"
	return &ExpressController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}
