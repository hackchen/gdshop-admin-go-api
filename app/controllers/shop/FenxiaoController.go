package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/FenxiaoService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// FenxiaoController 分销
type FenxiaoController struct {
	*ShopBaseController
}

func NewFenxiaoController(inputReq *BaseReq.I) *FenxiaoController {
	return &FenxiaoController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *FenxiaoController) Info(r *ghttp.Request) *response.JsonResponse {
	return FenxiaoService.GetConfig(r, c.Req)
}

func (c *FenxiaoController) Update(r *ghttp.Request) *response.JsonResponse {
	return FenxiaoService.SetConfig(r, c.Req)
}

func (c *FenxiaoController) AddGoods(r *ghttp.Request) *response.JsonResponse {
	return FenxiaoService.AddGoods(r, c.Req)
}

func (c *FenxiaoController) GetGoodsPage(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return FenxiaoService.GetGoodsPage(r, c.Req)
}

func (c *FenxiaoController) GetLevel(r *ghttp.Request) *response.JsonResponse {
	return FenxiaoService.GetLevel(r, c.Req)
}

func (c *FenxiaoController) SetLevel(r *ghttp.Request) *response.JsonResponse {
	return FenxiaoService.SetLevel(r, c.Req)
}

func (c *FenxiaoController) ChangeStatus(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return FenxiaoService.ChangeStatus(r, c.Req)
}

func (c *FenxiaoController) Surveys(r *ghttp.Request) *response.JsonResponse {
	return FenxiaoService.Surveys(r, c.Req)
}
