package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/DiyPageService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// DiyPageController 装修
type DiyPageController struct {
	*ShopBaseController
}

func NewDiyPageController(inputReq *BaseReq.I) *DiyPageController {
	return &DiyPageController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *DiyPageController) SetActivated(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return DiyPageService.SetActivated(r, c.Req)
}
