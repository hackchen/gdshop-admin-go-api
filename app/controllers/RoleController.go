package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/RoleService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type RoleController struct {
	*BaseController
}

func NewRoleController(inputReq *BaseReq.I) *RoleController {
	return &RoleController{
		&BaseController{Req: inputReq},
	}
}

func (c *RoleController) Add(r *ghttp.Request) *response.JsonResponse {
	if c.Req.AddReq != nil {
		if err := r.Parse(c.Req.AddReq); err != nil {
			return response.FailByRequestMessage(r, err.Error())
		}
	}
	return RoleService.Add(r, c.Req)
}

func (c *RoleController) Update(r *ghttp.Request) *response.JsonResponse {
	if c.Req.UpdateReq != nil {
		if err := r.Parse(c.Req.UpdateReq); err != nil {
			return response.FailByRequestMessage(r, err.Error())
		}
	}
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return RoleService.Update(r, c.Req)
}

func (c *RoleController) Info(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return RoleService.Info(r.GetCtx(), c.Req)
}
