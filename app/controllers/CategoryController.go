package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/CategoryReq"
	"gdshop-admin-go-api/app/service/BaseService"
	"gdshop-admin-go-api/app/service/CategoryService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type CategoryController struct {
	*BaseController
}

func NewCategoryController(inputReq *BaseReq.I) *CategoryController {
	return &CategoryController{
		&BaseController{Req: inputReq},
	}
}

func (c *CategoryController) Order(r *ghttp.Request) *response.JsonResponse {
	var parames *CategoryReq.Order
	if err := r.Parse(&parames); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	//parames.MemberId = token.GetLoginMemberId(r)
	return CategoryService.Order(r, parames)
}

func (c *CategoryController) Move(r *ghttp.Request) *response.JsonResponse {
	return BaseService.CategoryMove(r, c.Req.TableName, r.GetInt("category_id"), r.GetArray("ids"),
		"parent_id")
}

func (c *CategoryController) Delete(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return CategoryService.Delete(r, c.Req)
}
