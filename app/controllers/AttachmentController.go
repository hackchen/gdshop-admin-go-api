package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/AttachmentService"
	"gdshop-admin-go-api/app/service/BaseService"
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools/fileTypeTest"
	"github.com/gogf/gf/crypto/gmd5"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/os/gfile"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/grand"
	"strings"
)

type AttachmentController struct {
	*BaseController
}

func NewAttachmentController(inputReq *BaseReq.I) *AttachmentController {
	return &AttachmentController{
		&BaseController{Req: inputReq},
	}
}

func (c *AttachmentController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return AttachmentService.Page(r, c.Req)
}

func (c *AttachmentController) Upload(r *ghttp.Request) *response.JsonResponse {
	file := r.GetUploadFile("file")
	if file == nil {
		return response.FailByRequestMessage(r, "文件不能为空")
	}
	var oneByte int64 = 1048576 // 1MB
	// 默认允许上传5MB
	allowSizeConfig := g.Cfg().GetInt64("upload.AllowSize", 5)
	allowSize := allowSizeConfig * oneByte

	if file.Size > allowSize {
		return response.FailByRequestMessage(r, "文件大小不能超过"+gconv.String(allowSizeConfig)+"MB")
	}

	fileExt := gfile.Ext(file.Filename)
	fileMd5 := gmd5.MustEncrypt(file)
	fileName := grand.Letters(2) + grand.S(30) + fileExt
	OriginalName := file.Filename
	file.Filename = fileName
	filePreFix := g.Cfg().GetString("upload.WwwrootPath")
	url := "/upload/" + gtime.Now().Format("Ym") + "/" + gtime.Now().Format("d") + "/"

	// 检测文件类型
	allowTypes := g.Cfg().GetStrings("upload.AllowTypes")
	if !fileTypeTest.CheckFileType(file, allowTypes) {
		return response.FailByRequestMessage(r, "不支持当前类型，只允许上传"+strings.Join(allowTypes, ","))
	}
	save, err := file.Save(filePreFix + url)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	contentType, err := fileTypeTest.GetContentType(file)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	// 添加数据到数据库
	insertData := g.Map{
		"original_name": OriginalName,
		"save_name":     fileName,
		"save_path":     url,
		"url":           url + save,
		"extension":     strings.Trim(fileExt, "."),
		"mime":          contentType,
		"size":          file.Size,
		"md5":           fileMd5,
	}
	res := AttachmentService.UploadAdd(r, insertData)
	if res.Code != 0 {
		return res
	}
	resData := gconv.Map(res.Data)
	insertData["id"] = resData["id"]
	PhotoPreFix := g.Cfg().GetString("upload.PhotoPreFix")
	insertData["base_url"] = PhotoPreFix
	return response.SuccessByRequestMessageData(nil, "成功", insertData)
}

func (c *AttachmentController) GetBaseUrl(r *ghttp.Request) *response.JsonResponse {
	return AttachmentService.GetBaseUrl(r)
}

func (c *AttachmentController) Move(r *ghttp.Request) *response.JsonResponse {
	return BaseService.CategoryMove(r, c.Req.TableName, r.GetInt("group_id"), r.GetArray("ids"),
		"group_id")
}

func (c *AttachmentController) RemotePhoto(r *ghttp.Request) *response.JsonResponse {
	return AttachmentService.RemotePhoto(r)
}
