package FenxiaoResp

type GetConfig struct {
	IsApply            int    `json:"is_apply"`
	IsCommissionMoney  int    `json:"is_commission_money"`
	LevelConfig        int    `json:"level_config"`
	SelfPurchaseRebate int    `json:"self_purchase_rebate"`
	TextAccount        string `json:"text_account"`
	TextChild          string `json:"text_child"`
	TextConcept        string `json:"text_concept"`
	TextFenxiaoName    string `json:"text_fenxiao_name"`
	TextMyTeam         string `json:"text_my_team"`
	TextWithdraw       string `json:"text_withdraw"`
	WaitingPeriod      int    `json:"waiting_period"`
	Withdraw           int    `json:"withdraw"`
	WithdrawRate       int    `json:"withdraw_rate"`
}
