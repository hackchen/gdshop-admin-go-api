package FenxiaoResp

type GetMemberRelationship struct {
	One   *UserInfo `json:"one"`   // 为空说明不存在
	Two   *UserInfo `json:"two"`   // 为空说明不存在
	Three *UserInfo `json:"three"` // 为空说明不存在
}

type UserInfo struct {
	Rate          float64 `json:"rate"`
	LevelId       int     `json:"level_id"`
	FenxiaoUserId int     `json:"fenxiao_user_id"`
	MemberId      int     `json:"member_id"`
	Status        int     `orm:"status"     json:"status"` // 状态 1正常 0禁用
}
