package CommonResp

type AreaTree struct {
	Code      string      `json:"code"`
	Name      string      `json:"name"`
	Childrens []*AreaTree `json:"childrens"`
}
