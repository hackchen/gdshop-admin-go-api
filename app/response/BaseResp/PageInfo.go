package BaseResp

type PageInfo struct {
	HasMore     bool `json:"has_more"`      // 是否有下一页
	PageIndex   int  `json:"page"`          // 当前页
	PageSize    int  `json:"size"`          // 页大小
	TotalPage   int  `json:"total_page"`    // 总页数
	TotalCount  int  `json:"total"`         // 总条数
	IsFirstPage bool `json:"is_first_page"` // 是否第一页
	IsLastPage  bool `json:"is_last_page"`  // 是否最后一页
}
