package BaseReq

type PageInfo struct {
	Page     int `v:"min:0#当前页不能小于1" d:1` // 当前页
	PageSize int `p:"size" d:10`          // 页大小
}
