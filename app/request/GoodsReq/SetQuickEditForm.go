package GoodsReq

// 修改价格
type SetFormPrice struct {
	GoodsId    int
	OptionData *[]SetFormPriceItem
}

type SetFormPriceItem struct {
	Id             int `json:"id"`
	NewMarketPrice int `json:"market_price" p:"new_market_price"`
	NewSellPrice   int `json:"sell_price" p:"new_sell_price"`
	NewCostPrice   int `json:"cost_price" p:"new_cost_price"`
}

// 修改库存
type SetFormStock struct {
	GoodsId    int
	OptionData *[]SetFormStockItem
}
type SetFormStockItem struct {
	Id       int `json:"id"`
	NewStock int `json:"stock" p:"new_stock"`
}

// 修改排序
type SetFormDisplaySort struct {
	Id          int
	DisplaySort int
}
