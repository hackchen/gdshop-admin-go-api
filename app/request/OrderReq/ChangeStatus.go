package OrderReq

import "gdshop-admin-go-api/app/request/BaseReq"

type ChangeStatus struct {
	BaseReq.AdminLogin
	Type        string `json:"type"`
	OrderId     int    `json:"order_id"`
	ExpressId   int    `json:"express_id"`
	ExpressSn   string `json:"express_sn"`
	IsSplit     int    `json:"is_split"`
	OrderSubIds []int  `json:"order_sub_ids"`
	MemberId    int    `json:"member-id"`
	Payment     string `json:"payment"` // 支付方式
	Remark      string `json:"remark"`
	PayLogId    int    `json:"pay_log_id"` // pay_log 表的ID，通常用于微信或者支付宝支付成功后进行关联
}
