package OrderReq

import "gdshop-admin-go-api/app/request/BaseReq"

type SetPriceChange struct {
	BaseReq.AdminLogin
	DispatchPriceChange string `json:"dispatch_price_change"`
	OrderId             int    `json:"order_id"`
	TotalPriceChange    string `json:"total_price_change"`
}
