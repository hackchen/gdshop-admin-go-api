package OrderReq

type ApiSendExpress struct {
	//AdminId int    `json:"admin_id" v:"required|min:1#请输入会员ID|请输入会员ID"`
	OrderId   int    `json:"order_id" v:"required|min:1#请输入订单ID|请输入订单ID"`
	ExpressId int    `json:"express_id"`
	ExpressSn string `json:"express_sn"`
}
