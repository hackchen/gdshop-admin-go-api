package OrderReq

type MemberConfirmOrder struct {
	OrderId  int `json:"order_id"`
	MemberId int `json:"member_id"`
}
