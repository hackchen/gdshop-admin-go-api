package OrderReq

type MemberPay struct {
	MemberId int    `json:"member_id" v:"required|min:1#请输入会员ID|请输入会员ID"`
	OrderId  int    `json:"order_id" v:"required|min:1#请输入订单ID|请输入订单ID"`
	Payment  string `json:"payment" v:"required|min-length:1#请输入支付方式|请输入支付方式"`
}
