package OrderReq

import "gdshop-admin-go-api/app/request/BaseReq"

type ChangeRemark struct {
	BaseReq.AdminLogin
	Type    string `json:"type"`
	OrderId int    `json:"order_id"`
	Remark  string `json:"remark"`
}
