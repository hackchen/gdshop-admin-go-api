package OpenReq

type Login struct {
	Type       string
	CaptchaId  string
	Username   string
	Password   string
	VerifyCode string
}
