package FenxiaoGoodsLevelReq

type Update struct {
	OneRate   int     `json:"one_rate"`
	TwoRate   int     `json:"two_rate"`
	ThreeRate float64 `json:"three_rate"`
	GoodsId   int     `json:"goods_id"`
}
