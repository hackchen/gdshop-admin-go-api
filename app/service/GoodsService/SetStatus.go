package GoodsService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func getGoodsStatusByTypeStr(typeStr string) int {
	switch typeStr {
	case "up":
		return 1
	}

	return 0
}

func SetStatus(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	typeStr := r.GetString("type")
	_, err := toolsDb.GetUnSafaTableAddDeleteWhere(r.GetCtx(), "goods").Where(
		"id",
		gconv.SliceAny(req.Ids),
	).Update(g.Map{
		"status": getGoodsStatusByTypeStr(typeStr),
	})
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}

	return response.SuccessByRequestMessage(r, "成功")
}
