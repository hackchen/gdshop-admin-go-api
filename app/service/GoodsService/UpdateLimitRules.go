package GoodsService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"time"
)

func UpdateLimitRules(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()

	limitRules := r.GetString("limit_rules", "")
	if limitRules == "" {
		return response.FailByRequestMessage(nil, "limit_rules 不能为空")
	}

	_, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "goods_limit").Where("id", req.Id).Update(g.Map{
		"limit_rules": limitRules,
		"update_at":   time.Now().Unix(),
	})
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}

	return response.SuccessByRequestMessage(nil, "编辑成功")
}
