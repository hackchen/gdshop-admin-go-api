package GoodsService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/encoding/gjson"
	"github.com/gogf/gf/net/ghttp"
)

func GetLimitRules(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	v, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "goods_limit").Where("id", req.Id).Value("limit_rules")
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}

	content, err := gjson.LoadContent(v.String())
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}

	return response.SuccessByRequestMessageData(nil, "成功", content)
}
