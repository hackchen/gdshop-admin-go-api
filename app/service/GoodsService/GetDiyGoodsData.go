package GoodsService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/CategoryService"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/net/ghttp"
	"strings"
)

func GetDiyGoodsData(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	goodsIdsTmp := r.GetString("goods_ids")
	if goodsIdsTmp == "" {
		return response.FailByRequestMessage(r, "goods_ids 请求参数错误")
	}
	goodsIds := strings.Split(goodsIdsTmp, ",")
	ctx := r.Context()
	res, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "goods").Where("id IN (?)", goodsIds).All()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	resList := res.List()
	// 先取出分类
	cateArr, err := CategoryService.GetCategoryArrayType(ctx, "shop")
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	resList = GoodsPageHandle(cateArr, resList)

	return response.SuccessByRequestMessageData(nil, "获取成功",
		resList)
}
