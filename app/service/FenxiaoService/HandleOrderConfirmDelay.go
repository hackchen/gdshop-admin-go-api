package FenxiaoService

import (
	"context"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
	"time"
)

// HandleOrderConfirmDelay 处理订单 确认收货，延迟
func HandleOrderConfirmDelay(orderId int) {
	// 实现 思路
	// 1.判断 《提现等待期》 如果为 0 ，直接把 status 改为 99
	// 2. 如果是《提现等待期》 大于 0 ，先把  status 改为 90，做一个延迟任务，到时间后修改 status 改为 99
	ctx := context.Background()
	config, err := GetMapConfig()
	if err != nil {
		return
	}
	if config.WaitingPeriod < 1 {
		// 无等待期，调用完成
		HandleOrderConfirm(orderId)
	} else {
		// 有等待期
		err := g.DB().Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
			_, err := tx.Model("fenxiao_order").Unscoped().Where(
				"status = 1 AND order_id = ?", orderId).Update(g.Map{
				"status":             90,
				"settle_accounts_at": time.Now().Unix() + int64(86400*config.WaitingPeriod),
			})
			if err != nil {
				return err
			}
			// status = 1 才更新
			_, err = tx.Model("fenxiao_order_details").Unscoped().Where(
				"status = 1 AND order_id = ?", orderId).Update(g.Map{
				"status": 90,
			})
			if err != nil {
				return err
			}

			return nil
		})
		if err != nil {
			return
		}
		// 没有错误，加入延迟任务，调用 HandleOrderConfirm 完成方法

	}
}
