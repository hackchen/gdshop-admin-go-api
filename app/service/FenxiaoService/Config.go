package FenxiaoService

import (
	"context"
	"gdshop-admin-go-api/app/response/FenxiaoResp"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"gdshop-admin-go-api/library/tools/myCache"
	"github.com/gogf/gf/util/gconv"
	"time"
)

var fenxiaoConfigKey = "fenxiao"

func GetMapConfig() (*FenxiaoResp.GetConfig, error) {
	var data *FenxiaoResp.GetConfig
	value, err := myCache.GetOrSetFunc("sys_config:"+fenxiaoConfigKey, func() (interface{}, error) {
		nei, err := toolsDb.GetUnSafaTableAddDeleteWhere(context.Background(), "sys_config").Where(
			"config_key = ?", fenxiaoConfigKey).Value("config_value")
		if err != nil {
			return data, err
		}
		return nei.String(), nil
	}, 600*time.Second)
	if err != nil {
		return nil, err
	}
	err = gconv.Scan(value, &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// 配置 是否开启分销
// true 开启状态
func ConfigLevelConfig() bool {
	/*count, err := toolsDb.GetUnSafaTableAddDeleteWhere(context.Background(), "sys_config").Where(
		"config_key = ? AND config_value->'$.level_config' = 1", fenxiaoConfigKey).Count()
	if err != nil {
		return false
	}
	return count > 0*/

	config, err := GetMapConfig()
	if err != nil {
		return false
	}

	return config.LevelConfig == 1
}

// 配置 开启自购分佣，并且 开启分销
// true 开启状态
func ConfigSelfPurchaseRebate() bool {
	/*count, err := toolsDb.GetUnSafaTableAddDeleteWhere(context.Background(), "sys_config").Where(
		"config_key = ? AND config_value->'$.level_config' = 1 AND config_value->'$.self_purchase_rebate' = 1", fenxiaoConfigKey).Count()
	if err != nil {
		return false
	}
	return count > 0*/

	config, err := GetMapConfig()
	if err != nil {
		return false
	}

	return config.LevelConfig == 1 && config.SelfPurchaseRebate == 1
}
