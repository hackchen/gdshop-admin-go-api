package FenxiaoService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/encoding/gjson"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func GetConfig(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	configId := "fenxiao"
	value, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "sys_config").Where("config_key", configId).Value("config_value")
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}

	if value.IsEmpty() {
		return response.SuccessByRequestMessageData(r, "成功", g.Map{})
	}
	json, err := gjson.LoadContent(value)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return response.SuccessByRequestMessageData(r, "成功", json)
}
