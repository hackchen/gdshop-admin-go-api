package FenxiaoService

import (
	"gdshop-admin-go-api/library/tools"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
	"time"
)

func InvoiceChange(tx *gdb.TX, invoiceType, fenxiaoUserId, memberId, invoiceFee, srcId int, srcType string) error {
	invoiceNo, err := tools.GetInvoiceNo()
	if err != nil {
		return err
	}
	_, err = tx.Model("fenxiao_user_invoice").Unscoped().InsertAndGetId(g.Map{
		"fenxiao_user_id": fenxiaoUserId,
		"member_id":       memberId,
		"invoice_no":      invoiceNo,   // 流水号
		"invoice_type":    invoiceType, // 收入
		"invoice_fee":     invoiceFee,
		"src_type":        srcType,
		"src_id":          srcId,
		"create_at":       time.Now().Unix(),
	})
	if err != nil {
		return err
	}

	return nil
}
