package FenxiaoService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"time"
)

func SetConfig(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	configId := "fenxiao"
	_, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "sys_config").Where("config_key", configId).Save(g.Map{
		"config_key":   configId,
		"config_name":  "分销设置",
		"config_value": r.GetBodyString(),
		"update_at":    time.Now().Unix(),
	})
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	// 清除缓存
	g.Redis().Do("DEL", "sys_config:"+configId)
	return response.SuccessByRequestMessage(r, "设置成功")
}
