package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/request/OrderReq"
	"gdshop-admin-go-api/library/response"
)

// 关闭未支付订单
func CloseNoPayOrder(ctx context.Context, orderId int) *response.JsonResponse {
	orderModel := getOrderModel(ctx, orderId)
	if orderModel == nil {
		return response.FailByRequestMessage(nil, "找不到订单")
	}

	if orderModel.Status != 1 {
		// 当前状态已经不允许关闭，照样返回成功，删除任务
		return response.SuccessByRequestMessage(nil, "成功")
		//return response.FailByRequestMessage(nil, "订单当前不允许关闭")
	}

	jieguo := ChangeStatus(ctx, &OrderReq.ChangeStatus{
		OrderId: orderId,
		Type:    "close_order",
	})
	if jieguo.Code == 0 {
		// 如果关单成功，退还优惠券
		return GiveBackCouponByOrderId(ctx, orderId)
	}
	return jieguo
}
