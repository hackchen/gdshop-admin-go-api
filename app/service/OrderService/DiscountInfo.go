package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/entity"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/response/OrderResp"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
)

func DiscountInfo(ctx context.Context, req *BaseReq.I) *response.JsonResponse {
	var osList []*entity.OrderSub
	err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "order_sub").Where("order_id = ?", req.Id).Structs(&osList)
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	// 取出活动优惠
	var osActList []*entity.OrderSubActivity
	err = toolsDb.GetUnSafaTable(ctx, "order_sub_activity").As("osa").
		LeftJoin("goods_activity ga", "ga.id = osa.activity_id").
		Fields("osa.*, ga.activity_name").
		Where("osa.order_id = ?", req.Id).Structs(&osActList)
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	// 取出优惠券优惠
	var osCouponList []*entity.OrderSubCoupon
	err = toolsDb.GetUnSafaTable(ctx, "order_sub_coupon").As("osc").
		LeftJoin("coupon c", "c.id = osc.coupon_id").
		Fields("osc.*, c.title AS coupon_title,c.coupon_type").
		Where("osc.order_id = ?", req.Id).Structs(&osCouponList)
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}

	var discountInfo []*OrderResp.DiscountInfo
	for _, os := range osList {
		var discount *OrderResp.DiscountInfo
		discount = &OrderResp.DiscountInfo{
			OrderSub:  *os,
			Activitys: getDiscountInfoActivitys(osActList, os.Id),
			Coupons:   getDiscountInfoCoupons(osCouponList, os.Id),
		}
		discountInfo = append(discountInfo, discount)
	}

	return response.SuccessByRequestMessageData(nil, "获取成功",
		discountInfo)
}

// 从列表里面取出活动优惠
func getDiscountInfoActivitys(osActList []*entity.OrderSubActivity, orderSubId int) []*entity.OrderSubActivity {
	var activitys []*entity.OrderSubActivity
	for _, osAct := range osActList {
		if osAct.OrderSubId == orderSubId {
			activitys = append(activitys, osAct)
		}
	}
	if activitys != nil {
		return activitys
	}
	return []*entity.OrderSubActivity{}
}

// 从列表里面取出优惠券优惠
func getDiscountInfoCoupons(osCouponList []*entity.OrderSubCoupon, orderSubId int) []*entity.OrderSubCoupon {
	var coupons []*entity.OrderSubCoupon
	for _, osCoupon := range osCouponList {
		if osCoupon.OrderSubId == orderSubId {
			coupons = append(coupons, osCoupon)
		}
	}
	if coupons != nil {
		return coupons
	}
	return []*entity.OrderSubCoupon{}
}
