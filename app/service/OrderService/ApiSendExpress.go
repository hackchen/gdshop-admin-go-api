package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/OrderReq"
	"gdshop-admin-go-api/library/response"
)

// ApiSendExpress API发货
func ApiSendExpress(ctx context.Context, p *OrderReq.ApiSendExpress) *response.JsonResponse {
	orderModel := getOrderModel(ctx, p.OrderId)
	if orderModel == nil {
		return response.FailByRequestMessage(nil, "找不到订单")
	}
	if orderModel.Status != 3 {
		return response.FailByRequestMessage(nil, "订单当前不允许发货")
	}

	return ChangeStatus(ctx, &OrderReq.ChangeStatus{
		AdminLogin: BaseReq.AdminLogin{
			AdminId: -1,
		},
		OrderId:   p.OrderId,
		Type:      "send_express",
		ExpressId: p.ExpressId,
		ExpressSn: p.ExpressSn,
	})
}
