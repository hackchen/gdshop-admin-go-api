package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/request/OrderReq"
	"gdshop-admin-go-api/library/response"
)

// CompleteOrder 完成待收货订单，转成待评价
func CompleteOrder(ctx context.Context, orderId int) *response.JsonResponse {
	orderModel := getOrderModel(ctx, orderId)
	if orderModel == nil {
		return response.FailByRequestMessage(nil, "找不到订单")
	}

	if orderModel.Status != 4 {
		// 不是待收货，照样返回成功，删除任务
		return response.SuccessByRequestMessage(nil, "成功")
		//return response.FailByRequestMessage(nil, "订单当前不允许关闭")
	}

	jieguo := ChangeStatus(ctx, &OrderReq.ChangeStatus{
		OrderId: orderId,
		Type:    "confirm_order",
	})
	return jieguo
}
