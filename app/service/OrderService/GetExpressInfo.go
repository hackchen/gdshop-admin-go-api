package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
)

func GetExpressInfo(ctx context.Context, req *BaseReq.I) *response.JsonResponse {
	one, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "order_express").Where("order_id", req.Id).One()
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}

	if one.IsEmpty() {
		return response.FailByRequestMessage(nil, "找不到数据")
	}
	oneMap := one.Map()
	return response.SuccessByRequestMessageData(nil, "成功", g.Map{
		"express_com": oneMap["express_com"],
		"express":     oneMap["express"],
		"express_sn":  oneMap["express_sn"],
	})
}
