package OrderService

import (
	"gdshop-admin-go-api/library/event"
	"github.com/gogf/gf/util/gconv"
)

// TriggerOrderEvent 触发订单事件
func TriggerOrderEvent(eventData event.M) {
	eventName := "gdshop.order."
	typeStr := gconv.String(eventData["type"])
	// 触发事件
	switch typeStr {
	case "back_pay", "member_pay":
		eventName += "pay_ok"
		break
	default:
		eventName += typeStr
		break
	}
	// 触发 事件
	event.Trigger(eventName, eventData)
}
