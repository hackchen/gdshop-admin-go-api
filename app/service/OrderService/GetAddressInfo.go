package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
)

func GetAddressInfo(ctx context.Context, req *BaseReq.I) *response.JsonResponse {
	return response.SuccessByRequestMessageData(nil, "成功", getAddressInfoByorderId(ctx, req.Id))
}
