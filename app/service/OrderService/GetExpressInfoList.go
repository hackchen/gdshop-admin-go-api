package OrderService

import (
	"context"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/frame/g"
)

func GetExpressInfoList(ctx context.Context, express_sn string) *response.JsonResponse {
	fanhui := g.List{}
	for i := 0; i < 10; i++ {
		fanhui = append(fanhui, g.Map{
			"time": "2019-05-29 17:15:13",
			"desc": "【北京市】 快件已在 【北京永丰】 签收, 签收人: 本人",
		})
	}
	return response.SuccessByRequestMessageData(nil, "成功", fanhui)
}
