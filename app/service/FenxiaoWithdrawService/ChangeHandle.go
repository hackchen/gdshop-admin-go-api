package FenxiaoWithdrawService

import (
	"context"
	"gdshop-admin-go-api/app/entity"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/FenxiaoService"
	"gdshop-admin-go-api/app/service/MemberService"
	"gdshop-admin-go-api/library/event"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"gdshop-admin-go-api/library/tools/mylog"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
	"github.com/syyongx/php2go"
	"time"
)

func ChangeHandle(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	status := r.GetInt("status", 0)
	if !php2go.InArray(status, []int{2, 90}) {
		return response.FailByRequestMessage(r, "status 的值不正确")
	}
	update, err := toolsDb.GetUnSafaTable(ctx, "fenxiao_withdraw").Where("status = 1 AND id = ?", req.Id).Update(g.Map{
		"status":      status,
		"handle_id":   req.AdminId,
		"handle_time": time.Now().Unix(),
	})
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	affected, err := update.RowsAffected()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	if affected > 0 {
		// 写入余额
		// 触发 事件
		event.Trigger("gdshop.fenxiao.tixian_ok", event.M{
			"type":                "balance", // 提现到哪，balance 余额
			"fenxiao_withdraw_id": req.Id,
		})
		return response.SuccessByRequestMessage(r, "成功")
	}
	return response.FailByRequestMessage(r, "失败")
}

// HandleTixian 提现
// fenxiaoWithdrawId 提现ID
// typeStr 提现到哪
func HandleTixian(fenxiaoWithdrawId int, typeStr string) {
	if typeStr == "balance" {
		// 提现到余额
		ctx := context.Background()
		var fwModel *entity.FenxiaoWithdraw
		err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "fenxiao_withdraw").Where(
			"status = 90 AND id = ?", fenxiaoWithdrawId).Scan(&fwModel)
		if err != nil {
			mylog.ErrorLog("gdshop.fenxiao.tixian_ok HandleTixian 查询 fenxiao_withdraw 失败 ", err.Error(), fenxiaoWithdrawId, typeStr)
			return
		}

		err = g.DB().Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
			// 修改订单状态
			_, err := tx.Model("fenxiao_withdraw").Where("id", fenxiaoWithdrawId).Update(g.Map{
				"status":        99,
				"complete_time": time.Now().Unix(),
			})
			if err != nil {
				return err
			}

			// 写入流水，支出
			err = FenxiaoService.InvoiceChange(tx, 2, fwModel.FenxiaoUserId, fwModel.MemberId,
				int(fwModel.AmountReceived), fwModel.Id, "fenxiao_withdraw")
			if err != nil {
				return err
			}

			err = MemberService.CoinChange(tx, "+", "money_coin", "fenxiao_withdraw",
				fwModel.MemberId, fenxiaoWithdrawId, gconv.Int(fwModel.AmountReceived), "分销提现："+fwModel.WithdrawNo,
				fwModel.HandleId, 0)
			if err != nil {
				return err
			}

			return nil
		})
		if err != nil {
			mylog.ErrorLog("gdshop.fenxiao.tixian_ok HandleTixian 事务 失败 ", err.Error(), fenxiaoWithdrawId, typeStr)
			return
		}

		return
	}

	mylog.ErrorLog("gdshop.fenxiao.tixian_ok HandleTixian 暂时不支持 typeStr = ", typeStr)
}
