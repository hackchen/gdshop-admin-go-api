package GoodsActivityService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/GoodsActivityReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/net/ghttp"
)

func GetRule(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	activityId := r.GetInt("activity_id", 0)
	if activityId < 1 {
		return response.FailByRequestMessage(nil, "活动ID不能为空")
	}
	var tmp *GoodsActivityReq.SetRule
	err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "goods_activity_rule").Where(
		"activity_id",
		activityId,
	).Scan(&tmp)
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	if tmp == nil {
		tmp = &GoodsActivityReq.SetRule{
			Rules: []GoodsActivityReq.RulesItem{},
		}
	}
	return response.SuccessByRequestMessageData(nil, "成功", tmp)
}
