package GoodsActivityService

import (
	"gdshop-admin-go-api/app/entity"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func SetRule(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()

	activityId := r.GetInt("activity_id")
	var gaModel *entity.GoodsActivity
	err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "goods_activity").Where("id", activityId).
		Scan(&gaModel)
	if err != nil {
		return response.FailByRequestMessage(r, "查询失败"+err.Error())
	}

	if gaModel == nil {
		return response.FailByRequestMessage(r, "找不到活动")
	}

	_, err = toolsDb.GetUnSafaTable(ctx, "goods_activity_rule").Data(g.Map{
		"activity_id": activityId,
		"loop":        r.GetInt("loop"),
		"rules":       r.GetString("rules"),
	}).Save()
	if err != nil {
		return response.FailByRequestMessage(r, "保存失败："+err.Error())
	}

	return response.SuccessByRequestMessage(r, "设置成功")
}
