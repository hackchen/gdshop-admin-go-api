package GoodsActivityService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func SetStatus(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	// 检查，如果没有设置规则，不允许改变状态
	count, err := toolsDb.GetUnSafaTable(ctx, "goods_activity_rule").Where("activity_id", gconv.SliceAny(req.Ids)).
		Fields("1").Count()
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}

	if count < 1 {
		return response.FailByRequestMessage(nil, "未设置规则，不允许改变状态")
	}

	_, err = toolsDb.GetUnSafaTableAddDeleteWhere(ctx, req.TableName).Where(
		"id",
		gconv.SliceAny(req.Ids),
	).Update(g.Map{
		"status": gdb.Raw("ABS(status-1)"),
	})
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return response.SuccessByRequestMessage(nil, "切换成功")
}
