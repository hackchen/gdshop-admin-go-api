package LogService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func Clear(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	_, err := toolsDb.GetUnSafaTable(r.GetCtx(), req.TableName).Where("id>0").Delete()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return response.SuccessByRequestMessageData(nil, "清除成功", g.Map{})
}
