package AdminService

import (
	"context"
)

func CheckPermsByAdminId(ctx context.Context, perms string, adminId int, isAdmin bool) bool {
	// 如果是超级管理员，直接通过
	if isAdmin {
		return true
	}
	peramArr, err := GetPermsByAdminId(ctx, adminId)
	if err != nil {
		return false
	}

	//fmt.Println("peramArr ",peramArr)

	for _, item := range peramArr {
		if item == perms {
			return true
		}
	}

	return false
}
