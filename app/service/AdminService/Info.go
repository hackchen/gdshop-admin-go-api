package AdminService

import (
	"context"
	"database/sql"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/util/gconv"
)

func Info(ctx context.Context, req *BaseReq.I) *response.JsonResponse {
	obj := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, req.TableName).WherePri(
		"id",
		req.Id,
	)
	// 过滤字段
	if len(req.InfoIgnoreProperty) > 0 {
		obj.Fields(obj.GetFieldsExStr(req.InfoIgnoreProperty))
	}
	res, err := obj.One()
	if err != nil {
		if err != sql.ErrNoRows {
			// 非 结果为空
			return response.FailByRequestMessage(nil, err.Error())
		}
	}
	result := res.Map()
	// 取部门
	resCon, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "sys_department").Where(
		"id",
		result["dept_id"],
	).Fields("name").Value()
	result["departmentName"] = resCon.String()
	// 取角色
	roleIds, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "admin_role").Where(
		"admin_id",
		result["id"],
	).Fields("role_id").Array()
	result["roleIdList"] = gconv.SliceInt(roleIds)
	result["avatar_profix"] = g.Cfg().GetString("upload.PhotoPreFix")
	return response.SuccessByRequestMessageData(nil, "获取成功",
		result)
}
