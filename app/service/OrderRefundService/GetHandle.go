package OrderRefundService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/response/OrderRefundResp"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

// 根据子订单ID，获取待处理
func GetHandle(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()

	var orList []*OrderRefundResp.GetHandleList
	err := toolsDb.GetUnSafaTable(ctx, "order_refund").As("orr").
		LeftJoin("admin a", "orr.handle_admin_id = a.id").
		Fields("orr.*,a.nickname AS handle_admin_name").
		Where("orr.delete_at < 1 AND orr.order_sub_id = ?", req.Id).
		Structs(&orList)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}

	var handleModel *OrderRefundResp.GetHandleList

	for k, item := range orList {
		if item.Status == 1 {
			handleModel = item
			orList = append(orList[:k], orList[k+1:]...)
		}
	}

	return response.SuccessByRequestMessageData(r, "处理成功", g.Map{
		"list":   orList,
		"handle": handleModel,
		"statusTextArr": map[int]string{
			0:  "正常",
			1:  "申请中",
			2:  "驳回",
			3:  "同意(待退款)",
			99: "完成（已退款）",
		},
	})
}
