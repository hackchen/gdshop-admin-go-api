package FenxiaoGoodsLevelService

import (
	"database/sql"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/net/ghttp"
)

func Info(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	obj := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, req.TableName).WherePri(
		"fenxiao_goods_id",
		req.Id,
	)
	// 过滤字段
	if len(req.InfoIgnoreProperty) > 0 {
		obj.Fields(obj.GetFieldsExStr(req.InfoIgnoreProperty))
	}
	if req.InfoBeforeFn != nil {
		respRes := req.InfoBeforeFn(r)
		if respRes.Code != 0 {
			return respRes
		}
	}
	res, err := obj.One()
	if err != nil {
		if err != sql.ErrNoRows {
			// 非 结果为空
			return response.FailByRequestMessage(nil, err.Error())
		}
	}
	if res == nil {
		return response.FailByRequestMessage(nil, "没有数据")
	}
	resMap := res.Map()
	if req.InfoAfterFn != nil {
		respData, err := req.InfoAfterFn(r, resMap)
		if err != nil {
			return response.FailByRequestMessage(nil, err.Error())
		}
		resMap = respData
	}
	return response.SuccessByRequestMessageData(nil, "获取成功",
		resMap)
}
