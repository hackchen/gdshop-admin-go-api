package AttachmentService

import (
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

// UploadAdd 上传成功后插入数据
func UploadAdd(r *ghttp.Request, insertData g.Map) *response.JsonResponse {
	// 处理 fun_id 附加数据
	insertData["fun_id"] = r.GetInt("fun_id", 0)
	// 处理 group_id 附加数据
	insertData["group_id"] = r.GetInt("group_id", 0)

	res, err := toolsDb.GetUnSafaTableAddDeleteWhere(r.GetCtx(), "attachment").Insert(insertData)
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	id, err := res.LastInsertId()
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return response.SuccessByRequestMessageData(nil, "添加成功", g.Map{
		"id": id,
	})
}
