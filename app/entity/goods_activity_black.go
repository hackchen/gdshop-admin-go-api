package entity

// GoodsActivityBlack is the golang structure for table gd_goods_activity_black.
type GoodsActivityBlack struct {
	ActivityId int `orm:"activity_id,primary" json:"activity_id"` // 活动ID
	MemberId   int `orm:"member_id"           json:"member_id"`   // 会员ID
}
