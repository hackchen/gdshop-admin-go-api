// ==========================================================================
// This is auto-generated by gf cli tool. You may not really want to edit it.
// ==========================================================================

package entity

// Order is the golang structure for table gd_order.
type Order struct {
	Id                    int    `orm:"id,primary"              json:"id"`                      //
	OrderNo               string `orm:"order_no"                json:"order_no"`                // 订单号
	MemberId              int    `orm:"member_id"               json:"member_id"`               // 会员ID
	StoreId               int    `orm:"store_id"                json:"store_id"`                // 店铺ID
	AddressId             int    `orm:"address_id"              json:"address_id"`              // 关联地址ID
	TotalPrice            int64  `orm:"total_price"             json:"total_price"`             // 订单总价 当前价格 优惠 改价后 单位分
	OriginalTotalPrice    int64  `orm:"original_total_price"    json:"original_total_price"`    // 原订单总价（商品合计，未包含任何优惠）单位分
	Paid                  int64  `orm:"paid"                    json:"paid"`                    // 已付 暂时不用，后面开放定金时启用 单位分
	DispatchPrice         int64  `orm:"dispatch_price"          json:"dispatch_price"`          // 应付 运费 单位分
	OriginalDispatchPrice int64  `orm:"original_dispatch_price" json:"original_dispatch_price"` // 运费 商品设置的运费 单位分 不允许修改
	Remark                string `orm:"remark"                  json:"remark"`                  // 买家留言 备注
	SellerRemark          string `orm:"seller_remark"           json:"seller_remark"`           // 卖家备注
	Status                int    `orm:"status"                  json:"status"`                  // 状态 0关闭 1待付款 2待备货 3待发货 4待收货 5待评价 99已完成
	IsSplit               int    `orm:"is_split"                json:"is_split"`                // 分批发货
	CreateAt              uint   `orm:"create_at"               json:"create_at"`               // 创建时间
	UpdateAt              uint   `orm:"update_at"               json:"update_at"`               // 更新时间
	DeleteAt              uint   `orm:"delete_at"               json:"delete_at"`               // 删除时间 0为正常
	RecipientName         string `orm:"recipient_name"          json:"recipient_name"`          // 收货人
	RecipientMobile       string `orm:"recipient_mobile"        json:"recipient_mobile"`        // 收货电话
	GoodsKey              string `orm:"goods_key"               json:"goods_key"`               // 商品缩略信息方便搜索
}

type OrderAndPrice struct {
	Id                    int    `orm:"id,primary"              json:"id"`                      //
	OrderNo               string `orm:"order_no"                json:"order_no"`                // 订单号
	MemberId              int    `orm:"member_id"               json:"member_id"`               // 会员ID
	StoreId               int    `orm:"store_id"                json:"store_id"`                // 店铺ID
	AddressId             int    `orm:"address_id"              json:"address_id"`              // 关联地址ID
	TotalPrice            int64  `orm:"total_price"             json:"total_price"`             // 订单总价 当前价格 优惠 改价后 单位分
	OriginalTotalPrice    int64  `orm:"original_total_price"    json:"original_total_price"`    // 原订单总价（商品合计，未包含任何优惠）单位分
	Paid                  int64  `orm:"paid"                    json:"paid"`                    // 已付 暂时不用，后面开放定金时启用 单位分
	DispatchPrice         int64  `orm:"dispatch_price"          json:"dispatch_price"`          // 应付 运费 单位分
	OriginalDispatchPrice int64  `orm:"original_dispatch_price" json:"original_dispatch_price"` // 运费 商品设置的运费 单位分 不允许修改
	Remark                string `orm:"remark"                  json:"remark"`                  // 买家留言 备注
	SellerRemark          string `orm:"seller_remark"           json:"seller_remark"`           // 卖家备注
	Status                int    `orm:"status"                  json:"status"`                  // 状态 0关闭 1待付款 2待备货 3待发货 4待收货 5待评价 99已完成
	IsSplit               int    `orm:"is_split"                json:"is_split"`                // 分批发货
	CreateAt              uint   `orm:"create_at"               json:"create_at"`               // 创建时间
	UpdateAt              uint   `orm:"update_at"               json:"update_at"`               // 更新时间
	DeleteAt              uint   `orm:"delete_at"               json:"delete_at"`               // 删除时间 0为正常
	RecipientName         string `orm:"recipient_name"          json:"recipient_name"`          // 收货人
	RecipientMobile       string `orm:"recipient_mobile"        json:"recipient_mobile"`        // 收货电话
	GoodsKey              string `orm:"goods_key"               json:"goods_key"`               // 商品缩略信息方便搜索

	DispatchPriceChange int64 `orm:"dispatch_price_change" json:"dispatch_price_change"` // 运费 改价
	TotalPriceChange    int64 `orm:"total_price_change"    json:"total_price_change"`    // 总价 改价
}
