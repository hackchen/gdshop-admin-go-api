// ==========================================================================
// This is auto-generated by gf cli tool. You may not really want to edit it.
// ==========================================================================

package entity

// OrderSub is the golang structure for table gd_order_sub.
type OrderSub struct {
	Id               int    `orm:"id,primary"         json:"id"`                 //
	OrderId          int    `orm:"order_id"           json:"order_id"`           // 所属ID
	GoodsId          int    `orm:"goods_id"           json:"goods_id"`           // 所属商品ID
	StoreId          int    `orm:"store_id"           json:"store_id"`           // 所属店铺 0为自营
	GoodsName        string `orm:"goods_name"         json:"goods_name"`         // 商品名称
	SubTitle         string `orm:"sub_title"          json:"sub_title"`          // 副标题
	GoodsCover       string `orm:"goods_cover"        json:"goods_cover"`        // 商品封面
	GoodsThumbs      string `orm:"goods_thumbs"       json:"goods_thumbs"`       // 商品图片 第一张为缩略图，建议为正方型图片，其他为详情页面图片，尺寸建议宽度为640，并保持图片大小一致
	GoodsCates       string `orm:"goods_cates"        json:"goods_cates"`        // 分类 可多选，例如 ,1,2,
	MarketPrice      int64  `orm:"market_price"       json:"market_price"`       // 规格 原价 市场价 单位分
	SellPrice        int64  `orm:"sell_price"         json:"sell_price"`         // 规格 现价 销售价 单位分
	CostPrice        int64  `orm:"cost_price"         json:"cost_price"`         // 规格 成本价 单位分
	Total            int    `orm:"total"              json:"total"`              // 购买个数
	Paid             int    `orm:"paid"               json:"paid"`               // 子订单应付 （减去优惠后的） 单位分
	Discount         int    `orm:"discount"           json:"discount"`           // 优惠金额 单位分
	GoodsOptionId    int    `orm:"goods_option_id"    json:"goods_option_id"`    // 规格ID
	GoodsOptionTitle string `orm:"goods_option_title" json:"goods_option_title"` // 规格标题
	GoodsOptionThumb string `orm:"goods_option_thumb" json:"goods_option_thumb"` // 规格图片地址
	GoodsOptionSpecs string `orm:"goods_option_specs" json:"goods_option_specs"` // 规格唯一值
	Status           int    `orm:"status"             json:"status"`             // 子订单状态 0关闭 1待付款 2待备货 3待发货 4待收货 99已完成
	RefundStatus     int    `orm:"refund_status"      json:"refund_status"`      // 售后状态 0正常 1申请中 2驳回 3同意
	OrderSubExpress
	CouponDiscount   int `json:"coupon_discount"`
	ActivityDiscount int `json:"activity_discount"`
	OrderSubFenxiao
}

type OrderSubExpress struct {
	ExpressCom string `orm:"express_com"      json:"express_com"`
	Express    string `orm:"express"      json:"express"`
	ExpressSn  string `orm:"express_sn"      json:"express_sn"`
}

type OrderSubFenxiao struct {
	FoPaid               int64  `orm:"fo_paid"                  json:"fo_paid"`                  // 子订单应付 （减去优惠后的） 单位分 在订单支付后才会写入
	FoStatus             int64  `orm:"fo_status"                  json:"fo_status"`              // 子订单应付 （减去优惠后的） 单位分 在订单支付后才会写入
	OneRate              int    `orm:"one_rate"              json:"one_rate"`                    // 店内(自购)佣金比例
	OneLevelId           int    `orm:"one_level_id"          json:"one_level_id"`                // 店内(自购)等级ID
	OneFenxiaoUserId     int    `orm:"one_fenxiao_user_id"   json:"one_fenxiao_user_id"`         // 店内(自购)(fenxiao_user)ID
	OneMemberId          int    `orm:"one_member_id"         json:"one_member_id"`               // 店内(自购)(member)ID
	OneFenxiaoUserName   string `json:"one_fenxiao_user_name"`                                   // 店内(自购)(member)ID
	TwoRate              int    `orm:"two_rate"              json:"two_rate"`                    // 上级佣金比例
	TwoLevelId           int    `orm:"two_level_id"          json:"two_level_id"`                // 上级等级ID
	TwoFenxiaoUserId     int    `orm:"two_fenxiao_user_id"   json:"two_fenxiao_user_id"`         // 上级(fenxiao_user)ID
	TwoFenxiaoUserName   string `json:"two_fenxiao_user_name"`                                   // 上级(fenxiao_user)ID
	TwoMemberId          int    `orm:"two_member_id"         json:"two_member_id"`               // 上级(member)ID
	ThreeRate            int    `orm:"three_rate"            json:"three_rate"`                  // 上上级佣金比例
	ThreeLevelId         int    `orm:"three_level_id"        json:"three_level_id"`              // 上上级等级ID
	ThreeFenxiaoUserId   int    `orm:"three_fenxiao_user_id" json:"three_fenxiao_user_id"`       // 上上级(fenxiao_user)ID
	ThreeFenxiaoUserName string `json:"three_fenxiao_user_name"`                                 // 上上级(fenxiao_user)ID
	ThreeMemberId        int    `orm:"three_member_id"       json:"three_member_id"`             // 上上级(member)ID
	FoSettleAccountsAt   int    `orm:"fo_settle_accounts_at"       json:"fo_settle_accounts_at"` // 在此时间后可结算，用于有结算等待期的订单
}
