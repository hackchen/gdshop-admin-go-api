package entity

type PayRefund struct {
	Id            int    `orm:"id,primary"     json:"id"`             //
	OrderId       int    `orm:"order_id"       json:"order_id"`       // 所属订单ID
	RefundNo      string `orm:"refund_no"      json:"refund_no"`      // 我方退款单号
	PaymentMethod int    `orm:"payment_method" json:"payment_method"` // 支付方式 1微信 2支付宝
	TotalFee      int64  `orm:"total_fee"      json:"total_fee"`      // 订单总金额，单位为分，只能为整数
	RefundFee     int64  `orm:"refund_fee"     json:"refund_fee"`     // 退款总金额，订单总金额，单位为分，只能为整数
	RefundDesc    string `orm:"refund_desc"    json:"refund_desc"`    // 退款备注
	RefundId      string `orm:"refund_id"      json:"refund_id"`      // 微信退款单号
	RefundStatus  string `orm:"refund_status"  json:"refund_status"`  // SUCCESS-退款成功  CHANGE-退款异常  REFUNDCLOSE—退款关闭
	ErrCode       string `orm:"err_code"       json:"err_code"`       // 错误代码
	ErrCodeDes    string `orm:"err_code_des"   json:"err_code_des"`   // 错误代码描述
	Status        int    `orm:"status"         json:"status"`         // 状态 0提交中 1失败 99成功
	AdminId       int    `orm:"admin_id"       json:"admin_id"`       // 操作管理员
	MemberId      int    `orm:"member_id"      json:"member_id"`      // 操作会员
	UpdateTime    int64  `orm:"update_time"    json:"update_time"`    // 毫秒
}
