package entity

// GoodsActivityGift is the golang structure for table gd_goods_activity_gift.
type GoodsActivityGift struct {
	GoodsActivityId int `orm:"goods_activity_id,primary" json:"goods_activity_id"` //
	CouponId        int `orm:"coupon_id"                 json:"coupon_id"`         // 赠送优惠券ID 对应类型4
	GoodsId         int `orm:"goods_id"                  json:"goods_id"`          // 商品ID 对应类型5
	GoodsOptionId   int `orm:"goods_option_id"           json:"goods_option_id"`   // 商品规格ID 对应类型5
}
