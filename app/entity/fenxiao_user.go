package entity

type FenxiaoUser struct {
	Id             int    `orm:"id,primary" json:"id"`                      //
	MemberId       int    `orm:"member_id"  json:"member_id"`               // 所属用户ID
	UserNo         string `orm:"user_no"    json:"user_no"`                 // 分销商编号
	Account        string `orm:"account"    json:"account"`                 // 分销商账号
	Name           string `orm:"name"       json:"name"`                    // 分销商名称
	ParentId       int    `orm:"parent_id"  json:"parent_id"`               // 上级分销商ID
	ParentParentId int    `orm:"parent_parent_id"  json:"parent_parent_id"` // 上上级分销商ID
	LevelId        int    `orm:"level_id"   json:"level_id"`                // 分销等级ID
	Status         int    `orm:"status"     json:"status"`                  // 状态 1正常 0禁用
	IsVerify       int    `orm:"is_verify"  json:"is_verify"`               // 是否验证
	CreateAt       uint   `orm:"create_at"  json:"create_at"`               // 创建时间
	UpdateAt       uint   `orm:"update_at"  json:"update_at"`               // 更新时间
	DeleteAt       uint   `orm:"delete_at"  json:"delete_at"`               // 删除时间 0为正常
}
