package entity

// FenxiaoOrder is the golang structure for table gd_fenxiao_order.
type FenxiaoOrder struct {
	Id                 int    `orm:"id,primary"            json:"id"`                    //
	OrderId            int    `orm:"order_id"              json:"order_id"`              // 所属订单ID
	OrderSubId         int    `orm:"order_sub_id"          json:"order_sub_id"`          // 所属子订单ID
	Paid               int64  `orm:"paid"                  json:"paid"`                  // 子订单应付 （减去优惠后的） 单位分 在订单支付后才会写入
	FenxiaoType        int    `orm:"fenxiao_type"          json:"fenxiao_type"`          // 1默认规则 2单独设置
	OneRate            int    `orm:"one_rate"              json:"one_rate"`              // 店内(自购)佣金比例
	OneLevelId         int    `orm:"one_level_id"          json:"one_level_id"`          // 店内(自购)等级ID
	OneFenxiaoUserId   int    `orm:"one_fenxiao_user_id"   json:"one_fenxiao_user_id"`   // 店内(自购)(fenxiao_user)ID
	OneMemberId        int    `orm:"one_member_id"         json:"one_member_id"`         // 店内(自购)(member)ID
	TwoRate            int    `orm:"two_rate"              json:"two_rate"`              // 上级佣金比例
	TwoLevelId         int    `orm:"two_level_id"          json:"two_level_id"`          // 上级等级ID
	TwoFenxiaoUserId   int    `orm:"two_fenxiao_user_id"   json:"two_fenxiao_user_id"`   // 上级(fenxiao_user)ID
	TwoMemberId        int    `orm:"two_member_id"         json:"two_member_id"`         // 上级(member)ID
	ThreeRate          int    `orm:"three_rate"            json:"three_rate"`            // 上上级佣金比例
	ThreeLevelId       int    `orm:"three_level_id"        json:"three_level_id"`        // 上上级等级ID
	ThreeFenxiaoUserId int    `orm:"three_fenxiao_user_id" json:"three_fenxiao_user_id"` // 上上级(fenxiao_user)ID
	ThreeMemberId      int    `orm:"three_member_id"       json:"three_member_id"`       // 上上级(member)ID
	HandleStatus       int    `orm:"handle_status"         json:"handle_status"`         // 0待处理 1已处理 2失败
	HandleMsg          string `orm:"handle_msg"            json:"handle_msg"`            // 处理失败消息
	Status             int    `orm:"status"                json:"status"`                // 1冻结 2可结算
	CreateAt           uint   `orm:"create_at"             json:"create_at"`             // 创建时间
	UpdateAt           uint   `orm:"update_at"             json:"update_at"`             // 更新时间
	DeleteAt           uint   `orm:"delete_at"             json:"delete_at"`             // 删除时间 0为正常
}
