// ==========================================================================
// Code generated by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package entity

// FenxiaoGoods is the golang structure for table gd_fenxiao_goods.
type FenxiaoGoods struct {
	Id          int  `orm:"id,primary"   json:"id"`           //
	GoodsId     int  `orm:"goods_id"     json:"goods_id"`     //
	FenxiaoType int  `orm:"fenxiao_type" json:"fenxiao_type"` // 1默认规则 2单独设置
	Status      int  `orm:"status"       json:"status"`       // 状态 1启用
	CreateAt    uint `orm:"create_at"    json:"create_at"`    // 创建时间
	UpdateAt    uint `orm:"update_at"    json:"update_at"`    // 更新时间
	DeleteAt    int  `orm:"delete_at"    json:"delete_at"`    // 删除时间 0为正常
}
