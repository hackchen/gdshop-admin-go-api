package entity

// GoodsActivity is the golang structure for table gd_goods_activity.
type GoodsActivity struct {
	Id           int    `orm:"id,primary"    json:"id"`            //
	StoreId      int    `orm:"store_id"      json:"store_id"`      // 所属店铺 0为全场活动
	ActivityName string `orm:"activity_name" json:"activity_name"` // 活动名称
	ActivityTag  string `orm:"activity_tag"  json:"activity_tag"`  // 活动标签，不超过10个字，用于显示在商品详情和商品列表
	StartTime    int    `orm:"start_time"    json:"start_time"`    // 活动开始时间
	EndTime      int    `orm:"end_time"      json:"end_time"`      // 活动结束时间
	Remark       string `orm:"remark"        json:"remark"`        // 说明
	Status       int    `orm:"status"        json:"status"`        // 状态 1正常 0禁用
	Sort         int    `orm:"sort"          json:"sort"`          // 排序，如果一个商品有多个活动的话
	ActivityType int    `orm:"activity_type" json:"activity_type"` // 活动类型 1打折；2满减；3秒杀价；4送优惠券 5送赠品
	GoodsType    int    `orm:"goods_type"    json:"goods_type"`    // 商品类型 0全部 1包含 2排除
	GoodsIds     string `orm:"goods_ids"     json:"goods_ids"`     // 商品IDs
	CreateAt     uint   `orm:"create_at"     json:"create_at"`     // 创建时间
	UpdateAt     uint   `orm:"update_at"     json:"update_at"`     // 更新时间
	DeleteAt     uint   `orm:"delete_at"     json:"delete_at"`     // 删除时间 0为正常
}
