// ==========================================================================
// Code generated by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package entity

// FenxiaoUserInvoice is the golang structure for table gd_fenxiao_user_invoice.
type FenxiaoUserInvoice struct {
	Id            int    `orm:"id,primary"      json:"id"`              //
	FenxiaoUserId int    `orm:"fenxiao_user_id" json:"fenxiao_user_id"` // (fenxiao_user)ID
	MemberId      int    `orm:"member_id"       json:"member_id"`       // (member)ID
	InvoiceNo     string `orm:"invoice_no"      json:"invoice_no"`      // 流水号
	InvoiceType   int    `orm:"invoice_type"    json:"invoice_type"`    // 类型 1收入 2支出
	InvoiceFee    int64  `orm:"invoice_fee"     json:"invoice_fee"`     // 变动金额 可正可负
	CreateAt      uint   `orm:"create_at"       json:"create_at"`       // 创建时间
	DeleteAt      uint   `orm:"delete_at"       json:"delete_at"`       // 删除时间 0为正常
}
