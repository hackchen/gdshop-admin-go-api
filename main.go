package main

import (
	"fmt"
	_ "gdshop-admin-go-api/boot"
	_ "gdshop-admin-go-api/router"
	"github.com/gogf/gcache-adapter/adapter"
	"github.com/gogf/gf/frame/g"
)

func main() {
	//s := g.Server()
	//s.SetPort(8001)
	// 平滑重启，win不支持，先屏蔽掉
	//s.EnableAdmin()

	s := g.Server()
	adapterRedis := adapter.NewRedis(g.Redis())
	g.DB().GetCache().SetAdapter(adapterRedis)

	err := s.Start()
	if err != nil {
		panic(err)
	}
	staticServer := g.Server("static")
	staticServer.SetIndexFolder(true)
	staticServer.SetServerRoot(g.Cfg().GetString("upload.WwwrootPath"))
	staticServer.SetPort(g.Cfg().GetInt("upload.StaticServerPort"))
	err = staticServer.Start()
	if err != nil {
		panic(err)
	}

	fmt.Println("主服务， 路由个数:", len(s.GetRouterArray()))
	g.Wait()
}
