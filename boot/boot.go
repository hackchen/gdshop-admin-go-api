package boot

import (
	"gdshop-admin-go-api/app/service/FenxiaoService"
	"gdshop-admin-go-api/app/service/FenxiaoWithdrawService"
	"gdshop-admin-go-api/library/banner"
	"gdshop-admin-go-api/library/event"
	"gdshop-admin-go-api/library/tools"
	"gdshop-admin-go-api/library/tools/OrderQueue"
	"github.com/gogf/gf/os/gtimer"
	"github.com/gogf/gf/util/gconv"
	"time"
)

// 用于应用初始化。
func init() {
	// 添加代码层级的启动配置
	//s := g.Server()
	//s.Plugin(&swagger.Swagger{})
	banner.InitBanner()
	// 初始化订单队列
	OrderQueue.InitOrderQueue()
	// 跑一遍数据库，关闭一些非正常订单

	go OrderQueue.RedisCloseNoPayOrder()

	// 每 300 秒循环一次分销订单从可结算转换为已结算
	go func() {
		interval := 300 * 1400 * time.Millisecond
		gtimer.Add(interval, FenxiaoService.HandleOrderConfirmTask)
	}()

	claerCacheEvent := event.ListenerFunc(func(e event.Event) error {
		tools.ClaerCacheByKeyPrefix("adminCache:*")
		return nil
	})

	claerCacheNames := []string{
		"gdshop.admin.update",
		"gdshop.admin.personUpdate",
		"gdshop.role.add",
		"gdshop.role.update",
		"gdshop.role.delete",
		"gdshop.menu.add",
		"gdshop.menu.update",
		"gdshop.menu.delete",
	}

	for _, item := range claerCacheNames {
		event.On(item, claerCacheEvent, event.Normal)
	}
	// 支付完成事件，包含用户支付和后台支付
	event.On("gdshop.order.pay_ok", event.ListenerFunc(func(e event.Event) error {
		// 处理分销订单
		FenxiaoService.HandleOrderByOrderId(gconv.Int(e.Get("order_id")))
		return nil
	}), event.Normal)
	// 订单关闭事件
	event.On("gdshop.order.close_order", event.ListenerFunc(func(e event.Event) error {
		// 处理分销订单
		FenxiaoService.HandleOrderClose(gconv.Int(e.Get("order_id")))
		return nil
	}), event.Normal)
	// 确认收货后即可开始计算分销订单
	event.On("gdshop.order.confirm_order", event.ListenerFunc(func(e event.Event) error {
		// 处理分销订单
		FenxiaoService.HandleOrderConfirmDelay(gconv.Int(e.Get("order_id")))
		return nil
	}), event.Normal)
	// 处理提现审核通过后
	event.On("gdshop.fenxiao.tixian_ok", event.ListenerFunc(func(e event.Event) error {
		// 处理分销订单
		fenxiaoWithdrawId := gconv.Int(e.Get("fenxiao_withdraw_id"))
		typeStr := gconv.String(e.Get("type"))
		FenxiaoWithdrawService.HandleTixian(fenxiaoWithdrawId, typeStr)
		return nil
	}), event.Normal)
}
